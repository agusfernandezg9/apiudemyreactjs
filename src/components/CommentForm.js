import React from 'react';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {renderField} from "../form";
import Field from "redux-form/es/Field";
import {commentAdd} from "../actions/actions";

const mapDispatchToProps = {
    commentAdd
};

class CommentForm extends React.Component {
    onSubmit(values) {
        const {commentAdd, blogPostId, reset} = this.props;
        return commentAdd(values.content, blogPostId).then(() => reset());
    }

    render() {
        const {handleSubmit, submitting} = this.props;

        return (<div className="car mb-3 mt-3 shadow-sm">
            <div className="card-body">
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <Field name="content" label="Type you comment:" type="textarea" component={renderField}/>
                    <button type="submit" className="btn btn-primary btn-bg btn-block"
                            disabled={submitting}>
                        Add Comment
                    </button>
                </form>
            </div>
        </div>);
    }
}

export default reduxForm({
    form: 'CommentForm'
})(connect(null, mapDispatchToProps)(CommentForm));