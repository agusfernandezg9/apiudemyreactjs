import {
    COMMENT_ADDED,
    COMMENT_LIST_ERROR,
    COMMENT_LIST_RECEIVED,
    COMMENT_LIST_REQUEST,
    COMMENT_LIST_UNLOAD
} from "../actions/constants";
import {hydraPageCount} from "../apiUtils";

export default (state = {
    commentList: null,
    isFetching: false,
    currentPage: 1,
    pageCount: null
}, action) => {
    switch (action.type) {
        case COMMENT_LIST_REQUEST:
            state = {
                ...state,
                isFetching: true
            }
            return state;
        case COMMENT_LIST_RECEIVED:
            state = {
                ...state,
                commentList: !state.commentList ? action.data['hydra:member']
                    : state.commentList.concat(action.data['hydra:member']),
                isFetching: false,
                currentPage: state.currentPage + 1,
                pageCount: hydraPageCount(action.data)
            }
            return state;
        case COMMENT_ADDED:
            state = {
                ...state,
                commentList: [action.comment, ...state.commentList],
            }
            return state;
        case COMMENT_LIST_ERROR:
            state = {
                ...state,
                commentList: null,
                isFetching: false
            }
            return state;
        case COMMENT_LIST_UNLOAD:
            state = {
                ...state,
                commentList: null,
                isFetching: false,
                currentPage: 1,
                pageCount: null
            }
            return state;
        default:
            return state;
    }
}