import {USER_CONFIRMATION_SUCCESS, USER_REGISTER_COMPLETE, USER_REGISTER_SUCCESS} from "../actions/constants";

export default (state = {
    registrationSuccess: false,
    confirmationSuccess: false
}, action) => {
    switch (action.type) {

        case USER_REGISTER_SUCCESS:
            state = {
                ...state,
                registrationSuccess: true
            }
            return state;
        case USER_CONFIRMATION_SUCCESS:
            state = {
                ...state,
                confirmationSuccess: true
            }
            return state;
        case USER_REGISTER_COMPLETE:
            state = {
                ...state,
                confirmationSuccess: false,
                registrationSuccess: false
            }
            return state;
        default:
            return state;
    }
}