import {
    BLOG_POST_FORM_UNLOAD, IMAGE_DELETE_REQUEST, IMAGE_DELETED, IMAGE_UPLOAD_ERROR, IMAGE_UPLOAD_REQUEST,
    IMAGE_UPLOADED
} from "../actions/constants";

export default (state = {
    imageReqInProgress: false,
    images: []
}, action) => {
    switch (action.type) {
        case IMAGE_UPLOAD_REQUEST:
        case IMAGE_DELETE_REQUEST:
            state = {
                ...state,
                imageReqInProgress: true
            }
            return state;
        case IMAGE_UPLOADED:
            state = {
                ...state,
                imageReqInProgress: false,
                images: state.images.concat(action.image)
            }
            return state;
        case IMAGE_UPLOAD_ERROR:
            state = {
                ...state,
                imageReqInProgress: false
            }
            return state;
        case BLOG_POST_FORM_UNLOAD:
            state = {
                ...state,
                imageReqInProgress: false,
                images: []
            }
            return state;
        case IMAGE_DELETED:
            state = {
                ...state,
                imageReqInProgress: false,
                images: state.images.filter(image => image.id !== action.imageId)
            }
            return state;
        default:
            return state;
    }
}