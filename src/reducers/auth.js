import {USER_LOGIN_SUCCESS, USER_LOGOUT, USER_PROFILE_RECEIVED, USER_SET_ID} from "../actions/constants";

export default (state = {
    token: null,
    userId: null,
    isAuthenticated: false,
    userData: null
}, action) => {
    switch (action.type) {
        case USER_LOGIN_SUCCESS:
            state = {
                ...state,
                token: action.token,
                userId: action.userId,
                isAuthenticated: true
            }
            return state;
        case USER_SET_ID:
            state = {
                ...state,
                userId: action.userId
            }
            return state;
        case USER_PROFILE_RECEIVED:
            state = {
                ...state,
                userData: (state.userId === action.userId && state.userData == null)
                    ? action.userData : state.userData,
                isAuthenticated: (state.userId === action.userId && state.userData === null)
            }
            return state;
        case USER_LOGOUT:
            state = {
                ...state,
                token: null,
                userId: null,
                isAuthenticated: false,
                userData: null
            }
            return state;
        default:
            return state;
    }
}
